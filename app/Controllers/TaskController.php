<?php

namespace App\Controllers;

use App\Core\View;
use App\Models\Task;

class TaskController
{
    private $taskModel;

    public function __construct()
    {
        $this->taskModel = new Task();
    }

    public function index(array $request)
    {
        View::render('index', [
            'data' => $this->taskModel->all(),
            'user' => $_SESSION['user']
        ]);
    }

    public function create(array $request)
    {
        View::render('create', [
            'user' => $_SESSION['user']
        ]);
    }

    public function createTask(array $request)
    {
        $this->taskModel->create($request);

        header('Location: /');
    }

    public function edit(array $request)
    {
        $task = $this->taskModel->find($request['id']);
        View::render('edit', [
            'data' => $task
        ]);
    }

    public function update(array $request)
    {
        $this->taskModel->update($request);

        header('Location: /');
    }
}