<?php

namespace App\Controllers;

class UserController
{
    public function login(array $request)
    {
        if ($request == $_SESSION['env']['admin_access']) {
            $_SESSION['user'] = ['is_root' => true];
        }

        header('Location: /');
    }

    public function logout(array $request)
    {
        $_SESSION['user'] = ['is_root' => false];

        header('Location: ' . $_SERVER['HTTP_REFERER']);
    }
}