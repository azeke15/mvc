<?php

namespace App\Core;

class App
{
    protected $routes;

    public function __construct(array $routes)
    {
        $this->routes = $routes;
    }

    public function run()
    {
        $route = $_SERVER['REDIRECT_URL'];
        if (!isset($this->routes[$_SERVER['REQUEST_METHOD']][$route])) {
            throw new \Exception('Route does not exist');
        }

        $route_data = $this->routes[$_SERVER['REQUEST_METHOD']][$route];
        $controller = 'App\\Controllers\\' . $route_data[0];
        $action = $route_data[1];

        if (!class_exists($controller)) {
            throw new \Exception('Controller does not exist');
        }

        $objController = new $controller;

        if (!method_exists($objController, $action)) {
            throw new \Exception('action does not exist');
        }

        $objController->$action($_REQUEST);
    }
}