<?php

namespace App\Core;

class View
{
    public static function render(string $path, array $data = [])
    {
        $full_path = __DIR__ . '/../../resource/views/' . $path . '.php';
        if (!file_exists($full_path)) {
            throw new \Exception('View does not exists');
        }

        if (!empty($data)) {
            foreach ($data as $key => $value) {
                $$key = $value;
            }
        }

        include($full_path);
    }
}