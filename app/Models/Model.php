<?php

namespace App\Models;

use PDO;

abstract class Model
{
    protected $table_name;
    protected $connection;

    public function __construct()
    {
        $this->connection = new PDO("mysql:host={$_SESSION['connection']['host']};
            dbname={$_SESSION['connection']['database']}",
            $_SESSION['connection']['username'],
            $_SESSION['connection']['password']);
    }

    public function all() : array
    {
        return $this->connection->query("SELECT * FROM {$this->table_name}")
            ->fetchAll(PDO::FETCH_ASSOC);
    }

    public function create(array $params) : int
    {
        $columns = implode(', ', array_keys($params));
        $column_values = implode(', ', array_map(function ($item) {return ':' . $item;}, array_keys($params)));
        $id = $this->connection->prepare("INSERT INTO {$this->table_name} ({$columns})"
            ." VALUES ({$column_values})")->execute($params);

        return $id;
    }

    public function find(int $id)
    {
        return $this->connection->query("SELECT * FROM {$this->table_name} WHERE id = {$id}")
            ->fetch(PDO::FETCH_ASSOC);
    }

    public function update(array $params)
    {
        $id = $params['id'];
        unset($params['id']);
        $columns = implode(', ', array_map(function ($item) {return $item . '=?';}, array_keys($params)));
        $this->connection->prepare("UPDATE {$this->table_name} SET {$columns} WHERE id = {$id}")
            ->execute(array_values($params));
    }
}