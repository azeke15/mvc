<?php

function autoload(string $class_name) : void
{
    $class_name = ltrim($class_name, '\\');
    $file_name = '..\\';
    $namespace = '';
    if ($last_np_pos = strrpos($class_name, '\\')) {
        $namespace = substr($class_name, 0, $last_np_pos);
        $class_name = substr($class_name, $last_np_pos + 1);
        $file_name = '..\\' . str_replace('\\', DIRECTORY_SEPARATOR, $namespace) . DIRECTORY_SEPARATOR;
    }

    $file_name .= str_replace('_', DIRECTORY_SEPARATOR, $class_name) . '.php';
    require $file_name;
}

spl_autoload_register('autoload');