<?php

declare(strict_types=1);

require_once __DIR__ . '/../app/autoload.php';
$routes = require_once __DIR__ . '/../routes.php';
session_start();
$_SESSION['connection'] = require_once __DIR__ . '/../config/database.php';
$_SESSION['env'] = require_once __DIR__ . '/../env.php';
if (!isset($_SESSION['user'])) {
    $_SESSION['user'] = ['is_root' => false];
}

$app = new \App\Core\App($routes);
$app->run();
