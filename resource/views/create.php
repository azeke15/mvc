<?php require_once 'header.php' ?>

<div class="container" style="margin-top: 50px;">
    <h3>Create task</h3>
    <div class="row">
        <div class="col-md-12">
            <form method="POST" action="/task/add">
                <br>
                <input class="form-control" type="text" name="username" placeholder="Username" required />
                <br>
                <input class="form-control" type="email" name="email" placeholder="Email" required />
                <br>
                <textarea class="form-control" name="description" placeholder="Description" required></textarea>
                <br>
                <input type="hidden" name="status" value="0" />
                <input type="submit" value="Create" class="btn btn-primary">
            </form>
        </div>
    </div>
</div>

<?php require_once 'footer.php' ?>