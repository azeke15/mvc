<?php require_once 'header.php' ?>

    <div class="container" style="margin-top: 50px;">
        <h3>Edit task</h3>
        <div class="row">
            <div class="col-md-12">
                <form method="POST" action="/task/update">
                    <input type="hidden" name="id" value="<?=$data['id']?>">
                    <br>
                    <input class="form-control" type="text" name="username" readonly placeholder="Username" value="<?=$data['username']?>" required />
                    <br>
                    <input class="form-control" type="email" name="email" readonly placeholder="Email" value="<?=$data['email']?>" required />
                    <br>
                    <textarea class="form-control" name="description" placeholder="Description" required><?=$data['description']?></textarea>
                    <br>
                    <label>Status</label>
                    <br>
                    <?php if ($data['status'] == 0): ?>
                        <input type="radio" name="status" value="0" checked required> Open
                        <br>
                        <input type="radio" name="status" value="1" required> Closed
                    <?php else: ?>
                        <input type="radio" name="status" value="0" required> Open
                        <br>
                        <input type="radio" name="status" value="1" checked required> Closed
                    <?php endif; ?>
                    <br>
                    <input type="submit" value="Edit" class="btn btn-primary">
                </form>
            </div>
        </div>
    </div>

<?php require_once 'footer.php' ?>