<?php require_once 'header.php' ?>

<!-- Main jumbotron for a primary marketing message or call to action -->
<div class="jumbotron">
    <div class="container">
        <h1>JIRA</h1>
        <p><a class="btn btn-primary btn-lg" href="task/add" role="button">Create Task &raquo;</a></p>
    </div>
</div>

<div class="container">
    <!-- Example row of columns -->
    <tr class="row">
        <table id="tasks" class="display">
            <thead>
                <tr>
                    <th>Username</th>
                    <th>Email</th>
                    <th>Description</th>
                    <th>Status</th>
                    <?php if ($user['is_root'] == true): ?>
                        <th></th>
                    <?php endif; ?>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($data as $value): ?>
                    <tr>
                        <td><?=$value['username']?></td>
                        <td><?=$value['email']?></td>
                        <td><?=$value['description']?></td>
                        <td>
                            <?php if ($value['status'] == 0): ?>
                                <span style="color: red;">Open</span>
                            <?php else: ?>
                                <span style="color: green;">Closed</span>
                            <?php endif; ?>
                        </td>
                        <?php if ($user['is_root'] == true): ?>
                            <td>
                                <a href="/task/edit?id=<?=$value['id']?>" class="btn btn-primary">Edit</a>
                            </td>
                        <?php endif; ?>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>


</div> <!-- /container -->
<script>
    $(document).ready( function () {
        $('#tasks').DataTable({
            "pageLength": 3,
            "lengthMenu": [3, 6, 9],
            "info": false,
            "searching": false
        });
    } );
</script>

<?php require_once 'footer.php' ?>