<?php

return [

    'GET' => [
        '' => ['TaskController', 'index'],
        '/task/add' => ['TaskController', 'create'],
        '/user/logout' => ['UserController', 'logout'],
        '/task/edit' => ['TaskController', 'edit']
    ],

    'POST' => [
        '/task/add' => ['TaskController', 'createTask'],
        '/user/login' => ['UserController', 'login'],
        '/task/update' => ['TaskController', 'update']
    ]

];
